<?php $this->layout('default', ['title' => 'Fale Conosco - Netshow.me']) ?>

<h1>Entre em contato</h1>

<p>Mande a sua mensagem para nós. Iremos responder em breve!</p>

<div class="row">
    <div class="col-sm-6">
        <form method="post" action="<?= $this->pathFor('contact') ?>" enctype="multipart/form-data" novalidate>
            <?php
            $this->form()->setDefaultData($data);
            $this->form()->setErrors($errors);
            ?>
            <div class="form-group">
                <?= $this->form()->label('Nome completo', 'name') ?>
                <?= $this->form()->input('name', ['type' => 'text', 'class' => 'form-control']) ?>
                <?= $this->form()->error('name') ?>
            </div>
            <div class="form-group">
                <?= $this->form()->label('E-mail', 'email') ?>
                <?= $this->form()->input('email', ['type' => 'email', 'class' => 'form-control', 'placeholder' => 'seuemail@example.com']) ?>
                <?= $this->form()->error('email') ?>
            </div>
            <div class="form-group">
                <?= $this->form()->label('Telefone', 'phone') ?>
                <?= $this->form()->input('phone', ['type' => 'tel', 'class' => 'form-control', 'placeholder' => 'Número com DDD']) ?>
                <?= $this->form()->error('phone') ?>
            </div>
            <div class="form-group">
                <?= $this->form()->label('Mensagem', 'message') ?>
                <?= $this->form()->textarea('message', ['class' => 'form-control', 'rows' => '6']) ?>
                <?= $this->form()->error('message') ?>
            </div>
            <div class="form-group">
                <?= $this->form()->label('Arquivo Anexo', 'attachment_file') ?>
                <?= $this->form()->input('attachment_file', ['type' => 'file', 'class' => 'form-control-file']) ?>
                <?= $this->form()->error('attachment_file') ?>
            </div>
            <button type="submit" class="btn btn-primary">Enviar</button>
        </form>
    </div>
</div>
