<?php $messages = $this->flash(); ?>

<?php if (!empty($messages)): ?>
    <div class="row">
        <?php foreach ($messages as $type => $msgs): ?>
            <?php foreach ($msgs as $msg): ?>
                <div class="col-sm-6 alert alert-<?= $type ?>" role="alert">
                    <?= $msg ?>
                </div>
            <?php endforeach; ?>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
