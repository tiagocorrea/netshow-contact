<?php

use Phinx\Migration\AbstractMigration;

class UserMessage extends AbstractMigration
{
    public function change()
    {
        $this->table('user_messages')
            ->addColumn('name', 'string', ['length' => 128])
            ->addColumn('email', 'string', ['length' => 64])
            ->addColumn('phone', 'string', ['length' => 16])
            ->addColumn('message', 'text')
            ->addColumn('attachment', 'string', ['length' => 128])
            ->addColumn('ip_address', 'string', ['length' => 16])
            ->addColumn('created_at', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'update' => '',
                'timezone' => false,
            ])
            ->addIndex('email')
            ->create();
    }
}
