<?php declare(strict_types=1);

$checkProxyHeaders = false;
$app->add(new RKA\Middleware\IpAddress($checkProxyHeaders));
