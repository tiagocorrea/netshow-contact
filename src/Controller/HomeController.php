<?php declare(strict_types=1);

namespace App\Controller;

use Slim\Http\{Request, Response};

class HomeController extends AbstractController
{
    /**
     * @param \Slim\Http\Request $request
     * @param \Slim\Http\Response $response
     * @param array $args
     *
     * @return \Slim\Http\Response
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function index(Request $request, Response $response, array $args): Response
    {
        return $this->redirect($response, 'contact');
    }
}
