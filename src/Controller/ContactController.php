<?php declare(strict_types=1);

namespace App\Controller;

use App\Lib\Email;
use App\Lib\Exception\ValidationException;
use App\Model\UserMessage;
use Slim\Http\{Request, Response};

class ContactController extends AbstractController
{
    /**
     * @param \Slim\Http\Request $request
     * @param \Slim\Http\Response $response
     * @param array $args
     *
     * @return \Slim\Http\Response
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function index(Request $request, Response $response, array $args): Response
    {
        $data = $errors = [];
        if ($request->isPost()) {
            $data = $request->getParsedBody()['data'] ?? [];
            $data = array_map('trim', $data);
            $data['ip_address'] = $request->getAttribute('ip_address');

            try {
                $uploadedFiles = $request->getUploadedFiles();
                $uploadedFile = $uploadedFiles['data']['attachment_file'];
                $dir = $this->get('settings')['uploadDirectory'];
                $data['attachment_file'] = $uploadedFile;

                $this->get('contact_form_validator')->check($data);
                $filename = moveUploadedFile($dir, $uploadedFile);
                $data['attachment'] = $filename;
                UserMessage::createNew($data);
                $this->sendMail($data);

                $this->setFlash('Agradecemos a sua mensagem! Em breve o nosso suporte entrará em contato.', 'success');

                return $this->redirect($response, 'contact');
            } catch (ValidationException $e) {
                $errors = $e->getErrors();
                $this->setFlash('Existem erros no seu formulário. Por favor corrija-os e envie novamente.');
            } catch (\Exception $e) {
                $this->get('logger')->error($e->getMessage());
            } finally {
                $data['attachment_file'] = '';
            }

            return $this->render($response, 'contact', compact('data', 'errors'));
        }

        return $this->render($response, 'contact', compact('data', 'errors'));
    }

    /**
     * @param array $data
     *
     * @throws \Interop\Container\Exception\ContainerException
     */
    protected function sendMail(array $data)
    {
        $settings = $this->get('settings');
        $mailer = new Email($settings['email'], $settings['uploadDirectory']);
        $mailer->send([
            'to' => $settings['email']['to'],
            'subject' => 'Mensagem recebida',
            'view' => 'message_sent',
            'data' => $data,
        ]);
    }
}
