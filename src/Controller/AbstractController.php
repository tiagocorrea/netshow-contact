<?php declare(strict_types=1);

namespace App\Controller;

use Slim\Container;
use Slim\Http\Response;

abstract class AbstractController
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @param Container $container
     *
     * @return void
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $key
     *
     * @return mixed
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function get(string $key)
    {
        return $this->container->get($key);
    }

    /**
     * @param \Slim\Http\Response $response
     * @param string $template
     * @param array $args
     *
     * @return \Slim\Http\Response
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function render(Response $response, string $template, array $args = []): Response
    {
        return $this->get('view')
            ->setResponse($response)
            ->render($template, $args);
    }

    /**
     * @param string $msg
     * @param string $type
     *
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function setFlash(string $msg, string $type = 'danger')
    {
        $this->get('flash')->addMessage($type, $msg);
    }

    /**
     * @param \Slim\Http\Response $response
     * @param string $routeName
     * @param int $status
     *
     * @return \Slim\Http\Response
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function redirect(Response $response, string $routeName, int $status = null): Response
    {
        $router = $this->get('router');
        return $response->withRedirect($router->pathFor($routeName), $status);
    }
}
