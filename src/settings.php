<?php

require_once 'config.php';

defined('DS') ?: define('DS', DIRECTORY_SEPARATOR);

return [
    'settings' => [
        'displayErrorDetails' => DISPLAY_ERROR_DETAILS,
        'determineRouteBeforeAppMiddleware' => false,
        'addContentLengthHeader' => false,
        'timezone' => 'Etc/UTC',
        'uploadDirectory' => __DIR__ . '/../uploads',
        'acceptedMimeTypes' => [
            'txt' => 'text/plain',
            'pdf' => 'application/pdf',
            'doc' => 'application/msword',
            'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'odt' => 'application/vnd.oasis.opendocument.text',
        ],
        'maxUploadSize' => [
            'label' => '500 KB',
            'value' => 500 * 1024,
        ],

        // DB settings
        'db' => [
            'driver' => DB_DRIVER,
            'host' => DB_HOST,
            'database' => DB_NAME,
            'username' => DB_USER,
            'password' => DB_PASSWORD,
            'port' => DB_PORT,
            'charset' => DB_CHARSET,
            'collation' => DB_COLLATION,
        ],

        // Phinx config
        'phinx' => [
            'default' => [
                'adapter' => DB_DRIVER,
                'host' => DB_HOST,
                'name' => DB_NAME,
                'user' => DB_USER,
                'pass' => DB_PASSWORD,
                'port' => DB_PORT,
                'charset' => DB_CHARSET,
            ],
        ],

        // Renderer settings
        'view' => [
            'directory' => __DIR__ . '/../templates/',
        ],

        // Mail settings
        'email' => [
            'host' => SMTP_HOST,
            'port' => 587,
            'encryption' => 'tls',
            'from' => MAIL_FROM,
            'to' => MAIL_TO,
            'username' => SMTP_USERNAME,
            'password' => SMTP_PASSWORD,
            'templatesDir' => __DIR__ . '/../templates/email',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => Monolog\Logger::DEBUG,
        ],
    ],
];
