<?php declare(strict_types=1);

namespace App\Lib;

use League\Plates\Engine;

class Email
{
    /** @var array */
    protected $settings;

    /** @var string */
    protected $uploadDirectory;

    /**
     * @param array $settings
     * @param string $uploadDirectory
     */
    public function __construct(array $settings, string $uploadDirectory)
    {
        $this->settings = $settings;
        $this->uploadDirectory = $uploadDirectory;
    }

    /**
     * @param array $params
     *
     * @return int
     */
    public function send(array $params = []): int
    {
        $transport = new \Swift_SmtpTransport();

        $transport->setHost($this->settings['host'])
            ->setPort($this->settings['port'])
            ->setUsername($this->settings['username'])
            ->setPassword($this->settings['password'])
            ->setEncryption($this->settings['encryption']);

        $mailer = new \Swift_Mailer($transport);
        $message = new \Swift_Message();

        $message->setSubject($params['subject'])
            ->setFrom($params['from'] ?? $this->settings['from'])
            ->setTo($params['to']);

        $templates = new Engine($this->settings['templatesDir']);
        $content = $templates->render($params['view'], $params['data']);

        $message->setBody($content, $params['contentType'] ?? 'text/plain');

        $filePath = $this->uploadDirectory . DS . $params['data']['attachment'];
        $message->attach(\Swift_Attachment::fromPath($filePath));

        return $mailer->send($message);
    }
}
