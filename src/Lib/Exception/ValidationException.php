<?php declare(strict_types=1);

namespace App\Lib\Exception;

class ValidationException extends \Exception
{
    /** @var array */
    protected $errors = [];

    /**
     * @param array $errors
     *
     * @return \Exception
     */
    public function __construct(array $errors)
    {
        $this->errors = $errors;

        return parent::__construct('Data validation error(s).');
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
