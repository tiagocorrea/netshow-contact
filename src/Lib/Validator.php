<?php declare(strict_types=1);

namespace App\Lib;

use App\Lib\Exception\ValidationException;
use Valitron\Validator as Valitron;

abstract class Validator
{
    /**
     * @param Valitron $validator
     *
     * @return Valitron
     */
    abstract public function buildRules(Valitron $validator): Valitron;

    /**
     * @param array $data
     *
     * @return bool
     * @throws ValidationException
     */
    public function check(array $data): bool
    {
        $validator = new Valitron($data);
        $validator = $this->buildRules($validator);

        if ($validator->validate() === false) {
            throw new ValidationException($validator->errors());
        }

        return true;
    }
}
