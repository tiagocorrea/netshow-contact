<?php declare(strict_types=1);

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * UserMessage Model
 *
 * @property string name
 * @property string email
 * @property string phone
 * @property string message
 * @property string attachment
 * @property string ip_address
 * @property \DateTime created_at
 *
 */
class UserMessage extends Model
{
    /** @var array */
    public $fillable = ['name', 'email', 'phone', 'message', 'attachment', 'ip_address'];

    /** @var bool */
    public $timestamps = false;

    public static function createNew(array $data): UserMessage
    {
        return self::create($data);
    }
}
