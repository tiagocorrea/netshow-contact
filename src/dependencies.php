<?php declare(strict_types=1);

use App\Validator\Form\ContactFormValidator;
use App\View\Extension\{Flash, Form};
use Illuminate\Database\Capsule\Manager as DbManager;
use Projek\Slim\{Plates, PlatesExtension};
use Slim\Container;
use Slim\Flash\Messages;

$container = $app->getContainer();

$container['view'] = function (Container $c): Plates {
    $settings = $c->get('settings')['view'];
    $view = new Plates($settings);

    $view->setResponse($c->get('response'));

    $view->loadExtension(new PlatesExtension(
        $c->get('router'),
        $c->get('request')->getUri()
    ));

    $view->loadExtension(new Form(
        $c->get('request')->getParsedBody()['data'] ?? []
    ));

    $view->loadExtension(new Flash(new Messages()));

    return $view;
};

$capsule = new DbManager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$container['db'] = function () use ($capsule): DbManager {
    return $capsule;
};

$container['flash'] = function () {
    return new Messages();
};

$container['contact_form_validator'] = function (Container $c): ContactFormValidator {
    $maxUploadSize = $c->get('settings')['maxUploadSize'];
    $mimeTypes = $c->get('settings')['acceptedMimeTypes'];

    $validator = new ContactFormValidator();
    $validator->setFileMaxSize($maxUploadSize['value']);
    $validator->setFileMaxSizeLabel($maxUploadSize['label']);
    $validator->setAcceptedMimeTypes($mimeTypes);
    return $validator;
};

$container['logger'] = function (Container $c): Monolog\Logger {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};
