<?php declare(strict_types=1);

namespace App\View\Extension;

use League\Plates\Engine;
use League\Plates\Extension\ExtensionInterface;
use Slim\Flash\Messages;

class Flash implements ExtensionInterface
{
    /**
     * @var Messages
     */
    protected $flash;

    /**
     * @param Messages $flash
     */
    public function __construct(Messages $flash)
    {
        $this->flash = $flash;
    }

    /**
     * @param Engine $engine
     *
     * @return void
     */
    public function register(Engine $engine): void
    {
        $engine->registerFunction('flash', [$this, 'getMessages']);
    }

    /**
     * @param mixed $key
     *
     * @return array
     */
    public function getMessages($key = null): array
    {
        if ($key !== null) {
            return $this->flash->getMessage($key);
        }

        return $this->flash->getMessages();
    }
}
