<?php declare(strict_types=1);

namespace App\View\Extension;

use League\Plates\Engine;
use League\Plates\Extension\ExtensionInterface;

class Form implements ExtensionInterface
{
    /** @var array */
    protected $requestData = [];

    /** @var array */
    protected $defaultData = [];

    /** @var array */
    protected $errors = [];

    /** @var array */
    protected $defaultTemplates = [
        'label' => '<label for="{id}">{label}</label>',
        'error' => '<div class="invalid-feedback">{errors}</div>',
        'error_item' => '<div>{error}</div>',
        'input' => '<input type="{type}" name="data[{name}]" id="{id}" value="{value}" class="{class}"{extra}>',
        'select' => '<select name="data[{name}]" id="{id}" class="{class}"{extra}>{options}</select>',
        'select_option' => '<option value="{value}">{option}</option>',
        'textarea' => '<textarea name="data[{name}]" id="{id}" class="{class}" rows="{rows}"{extra}>{value}</textarea>',
    ];

    /** @var array */
    protected $userDefinedTemplates = [];

    /**
     * @param array $requestData
     *
     * @return void
     */
    public function __construct(array $requestData = [])
    {
        $this->requestData = $requestData;
    }

    /**
     * @param Engine $engine
     *
     * @return void
     */
    public function register(Engine $engine): void
    {
        $engine->registerFunction('form', [$this, 'getObject']);
    }

    /**
     * @return Form
     */
    public function getObject(): Form
    {
        return $this;
    }

    /**
     * @param string $label
     * @param string $name
     * @param string|null $id
     *
     * @return string
     */
    public function label(string $label, string $name = null, ?string $id = null): string
    {
        if ($name && !$id) {
            $id = camelize($name);
        }

        return self::format($this->getTemplate('label'), compact('label', 'id'));
    }

    /**
     * @param array $errors
     *
     * @return void
     */
    public function setErrors(array $errors): void
    {
        $this->errors = $errors;
    }

    /**
     * @param string $name
     *
     * @return string
     */
    public function error(string $name): string
    {
        if (!isset($this->errors[$name])) {
            return '';
        }

        $params = ['errors' => []];

        foreach ($this->errors[$name] as $error) {
            $params['errors'][] = self::format($this->getTemplate('error_item'), compact('error'));
        }

        $params['errors'] = join('', $params['errors']);

        return self::format($this->getTemplate('error'), $params);
    }

    /**
     * @param array $defaultParams
     * @param array $params
     *
     * @return array
     */
    protected function makeParams(array $defaultParams, array $params): array
    {
        $extra = array_filter($params, function ($v, $k) use ($defaultParams): bool {
            return !in_array($k, array_keys($defaultParams));
        }, \ARRAY_FILTER_USE_BOTH);

        if (!empty($extra)) {
            $extraAttributes = [];

            foreach ($extra as $k => $v) {
                $extraAttributes[] = sprintf('%s="%s"', $k, $v);
                unset($params[$k]);
            }

            $params['extra'] = ' ' . join(' ', $extraAttributes);
        }

        $params = $params + $defaultParams;
        $params['id'] = $params['id'] ?? camelize($params['name']);
        $params['value'] = $this->requestData[$params['name']]
            ?? $this->defaultData[$params['name']]
            ?? $params['value']
            ?? null;

        if (!empty($this->errors[$params['name']])) {
            $class = explode(' ', $params['class']);
            $class[] = 'is-invalid';
            $params['class'] = join(' ', $class);
        }

        return $params;
    }

    /**
     * @param string $name
     * @param array $params
     *
     * @return string
     */
    public function input(string $name, array $params = []): string
    {
        $defaultParams = [
            'class' => '',
            'extra' => '',
            'id' => null,
            'name' => $name,
            'type' => 'text',
            'value' => null,
        ];

        $params = $this->makeParams($defaultParams, $params);

        return self::format($this->getTemplate('input'), $params);
    }

    /**
     * @param string $name
     * @param array $params
     *
     * @return string
     */
    public function select(string $name, array $params = []): string
    {
        $defaultParams = [
            'class' => '',
            'extra' => '',
            'id' => null,
            'name' => $name,
            'options' => [],
            'value' => null,
        ];

        $params = $this->makeParams($defaultParams, $params);
        $options = [];

        foreach ($params['options'] as $k => $option) {
            $opt = self::format($this->getTemplate('select_option'), ['value' => $k, 'option' => $option]);

            if ($params['value'] == $k) {
                $dom = new \DOMDocument();

                $dom->loadHTML($opt);

                $option = $dom->getElementsByTagName('option')->item(0);

                if (!$option) {
                    continue;
                }

                $attr = $dom->createAttribute('selected');

                $attr->value = 'selected';

                $option->appendChild($attr);

                $html = $dom->saveHTML($option);

                if (!$html) {
                    continue;
                }

                $opt = str_replace("\n", '', $html);
            }

            $options[] = $opt;
        }

        $params['options'] = join('', $options);

        return self::format($this->getTemplate('select'), $params);
    }

    public function textarea(string $name, array $params = []): string
    {
        $defaultParams = [
            'class' => '',
            'extra' => '',
            'id' => null,
            'name' => $name,
            'rows' => '',
        ];

        $params = $this->makeParams($defaultParams, $params);

        return self::format($this->getTemplate('textarea'), $params);
    }

    /**
     * @param string $name
     *
     * @return string
     */
    protected function getTemplate(string $name): string
    {
        return $this->userDefinedTemplates[$name] ?? $this->defaultTemplates[$name] ?? '';
    }

    /**
     * @param string $name
     * @param string $template
     *
     * @return void
     */
    public function setTemplate(string $name, string $template): void
    {
        $this->userDefinedTemplates[$name] = $template;
    }

    /**
     * @param array $array
     *
     * @return void
     *
     */
    public function setRequestData(array $array): void
    {
        $this->requestData = $array;
    }

    /**
     * @param array $data
     *
     * @return void
     */
    public function setDefaultData(array $data): void
    {
        $this->defaultData = $data;
    }

    /**
     * @return void
     */
    public function resetRequestData(): void
    {
        $this->requestData = [];
    }

    /**
     * @return void
     */
    public function resetDefaultData(): void
    {
        $this->defaultData = [];
    }

    /**
     * @return void
     */
    public function resetErrors(): void
    {
        $this->errors = [];
    }

    /**
     * @return void
     */
    public function resetTemplates(): void
    {
        $this->userDefinedTemplates = [];
    }

    /**
     * @return void
     */
    public function reset(): void
    {
        $this->resetRequestData();
        $this->resetDefaultData();
        $this->resetErrors();
        $this->resetTemplates();
    }

    /**
     * @param string $template
     * @param array $vars
     *
     * @return string
     */
    private static function format(string $template, array $vars): string
    {
        $replace = [];

        foreach ($vars as $k => $v) {
            $replace['{' . $k . '}'] = $v;
        }

        return strtr($template, $replace);
    }
}
