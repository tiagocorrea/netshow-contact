<?php

define('DISPLAY_ERROR_DETAILS', false);

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOST', 'db');
define('DB_NAME', 'netshow');
define('DB_USER', 'root');
define('DB_PASSWORD', '');
define('DB_PORT', 3306);
define('DB_CHARSET', 'utf8');
define('DB_COLLATION', 'utf8_unicode_ci');

// Mail
define('MAIL_FROM', 'john@doe.com');
define('MAIL_TO', 'your@gmail.com');
define('SMTP_HOST', 'smtp_address');
define('SMTP_USERNAME', 'your@gmail.com');
define('SMTP_PASSWORD', 'secret');
