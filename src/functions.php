<?php declare(strict_types=1);

use Slim\Http\UploadedFile;

if (function_exists('moveUploadedFile') === false) {
    /**
     * Moves the uploaded file to the upload directory and assigns it a unique name
     * to avoid overwriting an existing uploaded file.
     *
     * @param string $directory directory to which the file is moved
     * @param UploadedFile $uploadedFile
     *
     * @return string filename of moved file
     * @throws \Exception
     * @see https://www.php.net/manual/en/function.random-bytes.php
     *
     */
    function moveUploadedFile(string $directory, UploadedFile $uploadedFile)
    {
        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        $basename = bin2hex(random_bytes(8));
        $filename = sprintf('%s.%0.8s', $basename, $extension);

        $uploadedFile->moveTo($directory . DS . $filename);

        return $filename;
    }
}

if (function_exists('camelize') === false) {
    /**
     * @param string $string
     *
     * @return string
     */
    function camelize(string $string): string
    {
        return str_replace(' ', '', ucwords(str_replace(['.', '_', '-'], ' ', $string)));
    }

}
