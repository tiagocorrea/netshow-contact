<?php

use App\Controller\{ContactController, HomeController};

$app->get('/', HomeController::class . ':index')->setName('home');

$app->map(['GET', 'POST'], '/contact', ContactController::class . ':index')->setName('contact');
