<?php declare(strict_types=1);

namespace App\Validator\Form;

use App\Lib\Validator;
use Slim\Http\UploadedFile;
use Valitron\Validator as Valitron;

class ContactFormValidator extends Validator
{
    /** @var int */
    protected $maxFileSize;

    /** @var string */
    protected $maxFileSizeLabel;

    /** @var array */
    protected $acceptedMimeTypes;

    /**
     * @param Valitron $v
     *
     * @return Valitron
     */
    public function buildRules(Valitron $v): Valitron
    {
        $v->rule('required', [
            'name',
            'email',
            'phone',
            'message',
        ])->message('Este campo é obrigatório.');

        $v->rule(function (string $field, string $value) {
            if (empty($value)) {
                return true;
            }

            return filter_var($value, FILTER_VALIDATE_EMAIL) !== false;
        }, 'email')
            ->message('E-mail inválido.');

        $v->rule(function (string $field, string $value) {
            if (empty($value)) {
                return true;
            }

            $sanitized = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
            $phoneNumber = str_replace('-', '', $sanitized);
            $length = strlen($phoneNumber);

            return in_array($length, [10, 11]);
        }, 'phone')
            ->message('O número de telefone é inválido.');

        $v->rule(function (string $field, UploadedFile $value) {
            if ($value->getError() == UPLOAD_ERR_NO_FILE) {
                return true;
            }

            $size = $value->getSize();
            return $size <= $this->maxFileSize;
        }, 'attachment_file')
            ->message(sprintf('O tamanho máximo do arquivo não deve ultrapassar %s.', $this->maxFileSizeLabel));

        $v->rule(function (string $field, UploadedFile $value) {
            if ($value->getError() == UPLOAD_ERR_NO_FILE) {
                return true;
            }

            return in_array($value->getClientMediaType(), array_values($this->acceptedMimeTypes));
        }, 'attachment_file')
            ->message(sprintf(
                'Por favor, envie um arquivo nos formatos %s.', implode(', ', array_keys($this->acceptedMimeTypes))
            ));

        $v->rule(function (string $field, UploadedFile $value) {
            return $value->getError() != UPLOAD_ERR_NO_FILE;
        }, 'attachment_file')
            ->message('Por favor, envie um arquivo.');

        return $v;
    }

    /**
     * @param int $size
     */
    public function setFileMaxSize(int $size)
    {
        $this->maxFileSize = $size;
    }

    /**
     * @param string $label
     */
    public function setFileMaxSizeLabel(string $label)
    {
        $this->maxFileSizeLabel = $label;
    }

    /**
     * @param array $types
     */
    public function setAcceptedMimeTypes(array $types)
    {
        $this->acceptedMimeTypes = $types;
    }
}
