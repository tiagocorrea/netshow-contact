<?php

namespace Tests\Functional;

class HomepageTest extends BaseTestCase
{
    public function testGetHomepagewithRedirect()
    {
        $userData = ['REQUEST_URI' => '/'];
        $response = $this->runApp($userData);

        $this->assertEquals(302, $response->getStatusCode());
    }
}
