<?php

namespace Tests\Functional;

class ContactTest extends BaseTestCase
{
    public function testGetContact()
    {
        $userData = ['REQUEST_URI' => '/contact'];
        $response = $this->runApp($userData);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString('Fale Conosco', (string) $response->getBody());
    }
}
