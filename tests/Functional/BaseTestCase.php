<?php

namespace Tests\Functional;

use PHPUnit\Framework\TestCase;
use Slim\App;
use Slim\Http\{Environment, Request, Response};

class BaseTestCase extends TestCase
{
    /**
     * Use middleware when running application?
     *
     * @var bool
     */
    protected $withMiddleware = true;

    /**
     * Process the application given a request
     *
     * @param array|null $userData
     * @param array|object|null $requestData the request data
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Slim\Exception\MethodNotAllowedException
     * @throws \Slim\Exception\NotFoundException
     */
    public function runApp($userData = null, $requestData = null)
    {
        $settings = require __DIR__ . '/../../src/settings.php';

        $app = new App($settings);

        require __DIR__ . '/../../src/dependencies.php';

        if ($this->withMiddleware) {
            require __DIR__ . '/../../src/middleware.php';
        }

        require __DIR__ . '/../../src/routes.php';

        $defaultUserData = [
            'REQUEST_METHOD' => 'GET',
        ];

        $userData = array_merge($defaultUserData, $userData);
        $environment = Environment::mock($userData);

        $request = Request::createFromEnvironment($environment);

        if (isset($requestData)) {
            $request = $request->withParsedBody($requestData);
        }

        $response = new Response();
        $response = $app->process($request, $response);


        return $response;
    }
}
