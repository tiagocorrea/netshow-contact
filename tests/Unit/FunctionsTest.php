<?php declare(strict_types=1);

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

require __DIR__ . '/../../src/functions.php';

class FunctionsTest extends TestCase
{
    public function testCamelize(): void
    {
        $text = 'hola_from.brazil.foo-bar';
        $expected = 'HolaFromBrazilFooBar';
        $camelized = camelize($text);

        $this->assertEquals($expected, $camelized);
    }
}
