<?php declare(strict_types=1);

namespace Tests\Unit\Form;

use App\Lib\Exception\ValidationException;
use App\Validator\Form\ContactFormValidator;
use PHPUnit\Framework\TestCase;
use Slim\Http\UploadedFile;

class ContactFormValidatorTest extends TestCase
{
    /** @var \App\Validator\Form\ContactFormValidator */
    protected $validator;

    /** @var string */
    protected $uploadDir;

    protected function setUp(): void
    {
        $settings = require __DIR__ . '/../../../src/settings.php';
        $settings = $settings['settings'];
        $maxUploadSize = $settings['maxUploadSize'];
        $mimeTypes = $settings['acceptedMimeTypes'];

        $this->validator = new ContactFormValidator();
        $this->validator->setFileMaxSize($maxUploadSize['value']);
        $this->validator->setFileMaxSizeLabel($maxUploadSize['label']);
        $this->validator->setAcceptedMimeTypes($mimeTypes);

        $this->uploadDir = __DIR__ . '/../../uploads';
    }

    public function testSuccessCheck(): void
    {
        $filename = 'python3_sheet.pdf';
        $fullPath = $this->uploadDir . DS . $filename;
        $type = 'application/pdf';

        $data = [
            'name' => 'John Doe',
            'email' => 'john@doe.com.br',
            'phone' => '(11) 98765-1234',
            'message' => 'Hello!',
            'ip_address' => '192.168.0.5',
            'attachment_file' => new UploadedFile(
                $fullPath,
                $filename,
                $type,
                filesize($fullPath)
            )];

        $errors = [];
        try {
            $this->validator->check($data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();
        }

        $this->assertEmpty($errors);
    }

    public function testErrorCheck(): void
    {
        $filename = 'hearthstone_card_defs.xml';
        $fullPath = $this->uploadDir . DS . $filename;
        $type = 'text/xml';

        $data = [
            'name' => 'John Doe',
            'email' => 'john@doe',
            'phone' => '(11) 987 - 1234',
            'message' => 'Hello!',
            'ip_address' => '192.168.0.5',
            'attachment_file' => new UploadedFile(
                $fullPath,
                $filename,
                $type,
                filesize($fullPath)
            )];

        $errors = [];
        try {
            $this->validator->check($data);
        } catch (ValidationException $e) {
            $errors = $e->getErrors();
        }

        $this->assertArrayHasKey('email', $errors);
        $this->assertArrayHasKey('phone', $errors);
        $this->assertArrayHasKey('attachment_file', $errors);
        $this->assertArrayNotHasKey('name', $errors);
        $this->assertArrayNotHasKey('message', $errors);
        $this->assertArrayNotHasKey('ip_address', $errors);
    }
}
