## Netshow.me Contact

Primeiro é preciso configurar a aplicação, copiando o arquivo `src/config.dist.php` para um novo arquivo com o nome `src/config.php`

Será necessário definir as credenciais e o servidor SMTP para o envio dos e-mails. Se possuir uma conta no Gmail, será possível utilizar o SMTP do Gmail definindo uma App Password de acordo com as instruções neste [artigo](https://support.google.com/accounts/answer/185833). Um exemplo de configuração para envio de e-mails:

```
/* src/config.php */

<?php
// ...
define('MAIL_FROM', 'john@doe.com');
define('MAIL_TO', 'seu@gmail.com');
define('SMTP_HOST', 'smtp.gmail.com');
define('SMTP_USERNAME', 'seu@gmail.com');
define('SMTP_PASSWORD', 'app_password_criado');
```

A maneira mais rápida para rodar a aplicação é através do ambiente docker:

`$ docker-compose up -d`

Para instalar as dependências:

`$ docker-compose exec php composer install`

Enfim, execute as migrations para ter o DB pronto para uso:

`$ docker-compose exec php vendor/bin/phinx migrate`

Para rodar os testes, execute:

`$ docker-compose exec php composer test`

Obs.: O timestamp dos registros estão na timezone Etc/UTC.
